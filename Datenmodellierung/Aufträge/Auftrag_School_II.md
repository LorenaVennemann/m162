![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "School II": 2. Normalform

Sie erstellen die 2. Normalform aufgrund Daten in der 1. Normalform

#### Aufgabe

Nehmen sie die Musterlösung von der Aufgabe **School I** und bringen sie diese in die 2. Normalform.

Tasks:

- Erkennen sie die Entitäten und tragen sie diese in ein **konzeptionelles** Modell ein.
- Fügen sie die Kardinalitäten zwischen den Entitäten ein.
- Folgen sie den Schritten zur [Transformation von einem Konzeptionellen und Physischen Modell](../Theorie_Datenmodellierung.md#user-content-vom-logischen-zum-physischen-modell).

#### Zeit und Form

- 30 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162
