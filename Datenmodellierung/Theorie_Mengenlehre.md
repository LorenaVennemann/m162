![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Mengenlehre

[TOC]

## Symbole und Zeichen

| Symbol | Beschreibung |
| ------ | ------------ |
| Gross- und Klein-Buchstaben und ∈, ∉ | - Grossbuchstaben bezeichnen eine Menge. Im Beispiel unten gibt es die drei Mengen *G* (Grundmenge), *A*, und *B*<br />- Kleinbuchstaben bezeichnen Elemente, welche einer Menge zugewiesen sind (zumindest der Grundmenge)<br />- ∈ zeigt, dass ein Elemente in einer Menge enthalten ist, zum Beispiel: *a ∈ A*, *b ∈ A* oder *d ∈ B*<br />- ∉ zeigt, dass ein Element in einer Menge nicht enthalten ist, zum Beispiel: *a ∉ B* oder *d ∉ A*<br />![Mengenlehre-MengenundElemente](./x_gitressourcen/Mengenlehre-MengenundElemente.png) |
| {} oder Ø | Bezeichnet eine **leere Menge**. Im folgenden Beispiel ist die Menge *A* leer.<br />![Mengenlehre-LeereMenge](./x_gitressourcen/Mengenlehre-LeereMenge.png) |
| ⊂, ⊆   | ⊂ oder ⊆ bedeutet **Teilmenge** von. zum Beispiel: *B ⊂ A*<br />Dies ist der Fall, wenn eine Menge komplett in einer anderen Menge enthalten ist.<br />Für das Element *c* bedeutet dies, dass *c ∈ B* **und** *c ∈ A*<br /> ![Mengenlehre-Teilmenge](./x_gitressourcen/Mengenlehre-Teilmenge.png)<br /><br />**Achtung**: Es gibt einen Unterschied zwischen den beiden Zeichen, den wir hier aber ignorieren (echte und unechte Teilmengen). |
| ∩      | ∩ bezeichnet die **Schnittmenge** zwischen zwei Mengen, zum Beispiel: *A ∩ B*. Wenn zwei Mengen sich nicht überlagern, ist die Schnittmenge die leere Menge.<br />Für das Element *b* bedeutet dies, dass *b ∈ B* **und** *b ∈ A*<br />![Mengenlehre-Schnittmenge](./x_gitressourcen/Mengenlehre-Schnittmenge.png) |
| ∪      | ∪ bezeichnet die **Vereinigungsmenge** von zwei Mengen, zum Beispiel: *A ∪ B*. Die beiden Mengen müssen sich nicht überlagern.<br />Für ein Element x bedeutet dies, dass *x ∈ A* **oder** *x ∈ B*<br />![Mengenlehre-Vereinigungsmenge](./x_gitressourcen/Mengenlehre-Vereinigungsmenge.png) |
| X<sup>c</sup>    | X<sup>c</sup> bezeichnet die **Komplementärmenge**, also alle Elemente, die nicht in der Menge X enthalten sind, zum Beispiel: A<sup>c</sup> (alle Elemente im rot markierten Bereich).<br />Für ein Element x bedeutet dies, dass *x ∈ G* **und** *x ∉ A*<br />![Mengenlehre-Komplement](./x_gitressourcen/Mengenlehre-Komplement.png) |
| \ | \ bezeichnet die **Differenzmenge** zwischen zwei Mengen, zum Beispiel *B\A*. Es gehören alle Elemente dazu, die in der Menge B enthalten sind, aber nicht in der Menge A<br />Für ein Element x bedeutet dies, dass *x ∈ B **und** *x ∉ A*<br />![Mengenlehre-Differenz](./x_gitressourcen/Mengenlehre-Differenz.png) |

---

&copy;TBZ, 2022, Modul: m162