![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Datenstrukturen

[TOC]

Die folgenden Datenstrukturen sind in der Regel für die klassische imperative Programmierung entwickelt und optimiert worden. Andere Programmierparadigmen wie die funktionale Programmierung können durchaus andere Datenstrukturen erfordern.

## Datensatz / Record / Tupel

Datensätze (auch 'Tupel' oder engl. Record genannt) gehören zu den einfachsten Datenstrukturen. **Sie verkörpern Werte, die andere Werte enthalten**, üblicherweise in einer **fest definierten Anzahl und Folge**. Datensätze identifizieren sich meist durch eines oder mehrere der in ihnen enthaltenen Elemente, oft Datenfeld genannt.

**Beispiele**: 

- (ID=12, Vorname=Hans, Nachname=Wenger, Telefonnummer=07911111111, Adresse=Zuercherstrasse 12)
- [Länge=4.5, Song=Patience, Band=Guns'n'Roses]

Ob die Klammern rund oder eckig sind, spiel hier keine Rolle. Wichtig ist, dass die Daten thematisch zusammen gehören und normalerweise mehrere Tuples des gleichen Typs existieren. Ein Tuple/Record kann als eine Zeile einer Tabelle angeschaut werden.

## Array

Das Array ist eine der meist verwendeten Datenstrukturen. Es werden hierbei **mehrere Variablen vom selben Datentyp gespeichert**. Ein **Zugriff auf die einzelnen Elemente wird über einen Index möglich**. Jedes Feld kann wieder ein neues Array enthalten und bekommt dadurch eine zweite Dimension (zweidimensionales Array). Arrays sind aber keinesfalls nur auf zwei Dimensionen beschränkt, sondern werden beliebig mehrdimensional verwendet. Wegen ihrer Einfachheit und grundlegenden Bedeutung bieten die allermeisten Programmiersprachen eine konkrete Umsetzung dieser Datenstruktur als zusammengesetzten Datentyp Array im Grundsprachumfang an.

Einen Sonderfall bildet das assoziative Array (oft auch Dictionary genannt), bei dem nicht (nur) über einen numerischen Index, sondern über einen Schlüssel auf die gespeicherten Daten zugegriffen wird. Eine mögliche Art, ein assoziatives Array zu implementieren, ist die Hashtabelle.

<img src="images/array-example.png" alt="Array Beispiele" />

**Beispiele**:

- [12, 45, 23, 38, 28]
- ["Hans", "Werner", "Sabine", "Rafael", "Susanne"]
- ["pos01" = "Position 01", "pos02" = "Position 02", ...] => Hier handelt es sich um ein assoziatives Array und darum muss der Schlüssel mit angegeben werden. Also an der Position "pos01" befindet sich der Wert "Position 01"

**Analogie**: Take-Away Food Ketten mit Ziehnummern. 

## Verkettete Liste

Die verkettete Liste ist eine Datenstruktur zur **dynamischen Speicherung von beliebig vielen Objekten**. Dabei beinhaltet jedes Listenelement einer verketteten Liste als Besonderheit einen **Verweis auf das nächste Element**, wodurch die Gesamtheit der Objekte zu einer Verkettung von Objekten wird. Die zu einer Liste gehörenden Operationen sind relativ unspezifiziert. Sie werden oft in komplizierteren Datenstrukturen verwendet und meist direkt auf ihre Elemente zugegriffen.

Die Blockchain-Technologie verwendet eine solche Verknüpfung von Elementen (Blöcke).

**Beispiel**: http://www.codeadventurer.de/?p=1844
**Video-Erklärung**: https://studyflix.de/informatik/verkettete-liste-1433

<img src="images/verkettete_liste_knoten.jpg" alt="verkettete_liste_knoten" />

**Analogie**: Zug-Komposition. Die Wagen sind jeweils verbunden.

## Stapelspeicher / Stack

In einem Stapelspeicher (engl. stack oder ‚Kellerspeicher‘) kann eine beliebige Anzahl von Objekten gespeichert werden, jedoch können die gespeicherten Objekte **nur in umgekehrter Reihenfolge wieder gelesen werden**. Dies entspricht dem **LIFO-Prinzip**. F Zu einem Stapelspeicher gehören zumindest die Operationen

- push, um ein Objekt im Stapelspeicher abzulegen und
- pop, um das zuletzt gespeicherte Objekt wieder zu lesen und vom Stapel zu entfernen.

Weitere Optionen können top oder peek sein, um das oberste Element zu lesen, ohne es zu löschen.

**Beispiel**: https://de.wikipedia.org/wiki/Stapelspeicher
**Video-Erklärung**: https://studyflix.de/informatik/stacks-885

<img src="images/stapelspeicher.png" alt="stapelspeicher" />

**Analogie**: Einsteigen in Bus, Metro, etc. Der letzte der reingeht, muss oft als erster wieder aussteigen, damit die anderen durch können.

## Warteschlange

In einer Warteschlange (engl. queue) kann eine beliebige Anzahl von Objekten gespeichert werden, jedoch können die gespeicherten Objekte **nur in der gleichen Reihenfolge wieder gelesen werden**, wie sie gespeichert wurden. Dies entspricht dem **FIFO-Prinzip**. Zu einer Queue gehören zumindest die Operationen

- enqueue, um ein Objekt in der Warteschlange zu speichern und
- dequeue, um das zuerst gespeicherte Objekt wieder zu lesen und aus der Warteschlange zu entfernen.

Beispiel: https://de.wikipedia.org/wiki/Warteschlange_(Datenstruktur)

<img src="images/queue.png" alt="queue" />

**Analogie**: Warteschlage vor Club/Disko

## Vorrangwarteschlange

Eine Spezialisierung der Warteschlange ist die Vorrangwarteschlange, die auch Prioritätswarteschlange bzw. engl. **Priority Queue** genannt wird. Dabei wird vom FIFO-Prinzip abgewichen. Die Durchführung der enqueue-Operation, die in diesem Fall auch insert-Operation genannt wird, **sortiert das Objekt gemäß einer gegebenen Priorität**, die jedes Objekt mit sich führt, in die Vorrangwarteschlange ein. **Die dequeue-Operation liefert immer das Objekt mit der höchsten Priorität**. Vorrangwarteschlangen werden meist mit Heaps implementiert.

**Beispiel**: https://de.wikipedia.org/wiki/Vorrangwarteschlange

<img src="images/priority.png" alt="priority" />

**Analogie**: Warteschlage vor Club/Disko mit VIP-Liste

## Graph

Ein Graph ermöglicht es als Datenstruktur die Unidirektionalität der Verknüpfung zu überwinden und können **Referenzen auf mehrere Objekte** enthalten. Die Operationen sind auch hier das Einfügen, Löschen und Finden eines Objekts. 

**Beispiel:** https://de.wikipedia.org/wiki/Graph_(Graphentheorie)
**Video-Erklärung**: https://studyflix.de/informatik/grundbegriffe-der-graphentheorie-1285

<img src="images/graph.png" alt="graph" />

**Analogie**: GPS Navigationssystem.

## Baum

Bäume sind **spezielle Formen von Graphen** in der Graphentheorie. **Bäume besitzen nur eine eingehende Verknüpfung, aber mehrere ausgehende Verknüpfungen.**

Eine **Spezialform ist der Binärbäumen**, bei dem die Anzahl der Kinder höchstens zwei ist und in höhen-balancierten Bäumen angelegt wird, bei dem sich die Höhen des linken und rechten Teilbaums an jedem Knoten nicht zu sehr unterscheiden.

Bei **geordneten Bäumen, insbesondere Suchbäumen**, sind die Elemente in der Baumstruktur **geordnet abgelegt, sodass man schnell Elemente im Baum finden kann**.

**Beispiel**: https://de.wikipedia.org/wiki/Baum_(Graphentheorie)
**Video-Erklärung**: https://studyflix.de/informatik/binarbaum-1362

<img src="images/tree.png" alt="tree" />

## Heap

Der **Heap** (auch Halde oder Haufen) **vereint die Datenstruktur eines Baums mit den Operationen einer Vorrangwarteschlange**. Häufig hat der Heap neben den minimal nötigen Operationen wie insert, remove und extractMin auch noch weitere Operationen wie merge oder changeKey. 

**Beispiel**: https://de.wikipedia.org/wiki/Heap_(Datenstruktur)
**Video-Erklärung**: https://studyflix.de/informatik/heap-1440

<img src="images/heap.png" alt="heap" />

## Hashtabelle

Die Hashtabelle bzw. Streuwerttabelle ist eine **spezielle Indexstruktur, bei der die Speicherposition direkt berechnet werden kann**. Hashtabellen stehen dabei in Konkurrenz zu Baumstrukturen, die im Gegensatz zu Hashtabellen alle Indexwerte in einer Ordnung wiedergeben können, aber einen größeren Verwaltungsaufwand benötigen, um den Index bereitzustellen. Beim Einsatz einer Hashtabelle zur Suche in Datenmengen spricht man auch vom Hashverfahren. Bei sehr großen Datenmengen kann eine verteilte Hashtabelle zum Einsatz kommen.

**Beispiel**: https://de.wikipedia.org/wiki/Hashtabelle

<img src="images/hash.png" alt="hash" />

---

&copy;TBZ, 2022, Modul: m162